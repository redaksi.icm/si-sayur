
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>TENTANG KAMI</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">TENTANG KAMI</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start About Page  -->
    <div class="about-box-main">
        <div class="container">
            <div class="row">
				<div class="col-lg-6">
                    <div class="banner-frame"> <img class="img-fluid" src="images/blog-img-01.jpg" alt="" />
                    </div>
                </div>
                <div class="col-lg-6">
                    <h2 class="noo-sh-title-top"><span> Pasar Sayur</span></h2>
                  <p>Daerah Kampung Batu Dalam adalah salah satu kenagarian yang terdapat di Kecamatan  Danau  Kembar Kabupaten  Solok.Daerah  tersebut  merupakan  salah satu  daerah  terbesar  di  Sumatera  Barat  yang  menghasilkan  sayur-sayuran  untuk dikonsumsi  dan  didistribusikan  ke  daerah  lain. Walaupun  pendistribusian  sayur setiap  hari dilakukan  oleh daerah  ini,  promosi  sayurnya  belum  maksimal  karena masih dilakukan dengan promosi mulut ke mulut. Berdasarkan data yang tercatat pada Kantor Wali Nagari Kampung Batu Dalam hampir 80persen penduduknyabekerja  sebagai  petani  sayuran.Bersamaan  dengan  hal  itu  ada  juga  masyarakat yangmemiliki profesi sebagai agen atau toke sayuran.Dengan adanya agen sayur maka petani dimudahkan dengan proses penjualan sayur hingga sampaike tangan konsumen.</p>
                  <p>untuk membantu petani dan agen pada Daerah Kampung Batu Dalam dalam proses  jual beli sayur hingga sampai ketangan konsumen dan melakukan  promosi daerahserta  untuk mengikuti perkembangan  teknologi yang begitu  pesat  pada  saat  sekarangini,  penulis  membuat  sebuah  sistem  informasi yang  diberi  judul “Perancangan MarketplaceDagang  Sayur  Digital  pada Daerah Kampung Batu Dalam”.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- End About Page -->