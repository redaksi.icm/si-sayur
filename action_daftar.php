<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<link rel="stylesheet" href="sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
</head>
<body>
<?php 

include"database/koneksi.php";

$nama=$_POST['nama'];
$email=$_POST['email'];
$no_telepon=$_POST['no_telepon'];
$alamat=$_POST['alamat'];
$jekel=$_POST['jekel'];
$tgl_bergabung=date('Y-m-d');
$level=$_POST['level'];
$password=$_POST['password'];

if ($level=='petani') {
	$code=mysqli_query($koneksi,"SELECT max(id_petani) as code FROM tb_petani");
	$data=mysqli_fetch_array($code);
	$id=$data['code'];
	$urutan=(int) substr($id, 3,3);
	$urutan++;
	$huruf='P';
	$id=$huruf.sprintf("%03s",$urutan);
	$petani_sql=mysqli_query($koneksi,"INSERT INTO tb_petani VALUES ('$id','$nama','$email','$no_telepon','$alamat','$jekel','$tgl_bergabung','1')");
	$akun_petani=mysqli_query($koneksi,"INSERT INTO tb_login VALUES (NULL,'$id','$email','$password','$level')");
	if ($akun_petani&&$petani_sql==true) {
			echo "<script>
				setTimeout(function(){
						Swal.fire({
							icon:'success',
							title:'Selamat anda telah terdaftar sebagai Petani di Pasar Sayur!',
							type:'success',
							timer:'3000',
							showConfrimButton:'true'
							});
					},10);
					window.setTimeout(function(){
						window.location.replace('masuk');
						},3000);
			</script>";
	}
}elseif ($level=='konsumen') {
	$code=mysqli_query($koneksi,"SELECT max(id_konsumen) as code FROM tb_konsumen");
	$data=mysqli_fetch_array($code);
	$id=$data['code'];
	$urutan=(int) substr($id, 3,3);
	$urutan++;
	$huruf='K';
	$id=$huruf.sprintf("%03s",$urutan);
	$konsumen_sql=mysqli_query($koneksi,"INSERT INTO tb_konsumen VALUES ('$id','$nama','$email','$no_telepon','$alamat','$jekel','$tgl_bergabung','1')");
	$akun_konsumen=mysqli_query($koneksi,"INSERT INTO tb_login VALUES (NULL,'$id','$email','$password','$level')");
	if ($konsumen_sql&&$akun_konsumen==true) {
			echo "<script>
				setTimeout(function(){
						Swal.fire({
							icon:'success',
							title:'Selamat anda telah terdaftar jadi konsumen di Pasar Sayur',
							type:'success',
							timer:'3000',
							showConfrimButton:'true'
							});
					},10);
					window.setTimeout(function(){
						window.location.replace('masuk');
						},3000);
			</script>";
	}
}



 ?>
</body>
<script src="sweetalert2/sweetalert2.min.js"></script>
</html>