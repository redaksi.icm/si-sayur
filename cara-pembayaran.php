
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Cara Pembayaran</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Cara Pembayaran</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

      <!-- Start About Page  -->
    <div class="about-box-main">
        <div class="container">
            <div class="row">
				<div class="col-lg-3">
                   
                </div>
                <div class="col-lg-6">
                    <h2 class="noo-sh-title-top"><span> Cara Pembayaran:</span></h2>
                  <p>1. Konsumen melakukan pemesanan terlebih dahulu</p><br>
                  <p>2. Setelah melakukan pemesanan , konsumen terlebih dahulu menunggu sampai pemesanan berstatus "<b>Menunggu Pembayaran</b>"</p><br>
                  <p>3. Selanjutnya konsumen melakukan pembayaran dengan mentransfer ke nomor rekening <b>(066035334849237) (BRI)</b> dengan jumlah pembayaran yang tertera</p><br>
                  <p>4. Setelah konsumen mentransfer pembayaran, selanjutnya konsumen mengupload pembayaran pada tabel pemesanan.</p><br>
                  <p>5. Pembayaran akan dicek manual oleh admin.</p><br>
                  <p>6. Terakhir konsumen menuggu status pemesanan menjadi <b>"Sudah dibayar"</b> dan konsumen telah siap menjemput pemesanan ke petani dengan menyerahkan bukti pemesanan.</p><br>
                </div>
            </div>
        </div>
    </div>
    <!-- End About Page -->
