<?php 
include"database/koneksi.php";
$petani=mysqli_query($koneksi,"SELECT * FROM tb_petani WHERE id_petani='$id'");
$data_petani=mysqli_fetch_array($petani);

 ?>
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Akun Saya</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Akun Saya</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start My Account  -->
    <div class="my-account-box-main">
        <div class="container">
            <div class="my-account-page">
                    <div style="border:2px solid;border-radius:10px;padding: 20px;" align="center">
                        <i class="fa fa-user" style="font-size: 48px"></i>
                        <table style="text-align: center;">
                            <tr>
                                <td style="font-size: 20px"><?= $data_petani['nm_petani'];  ?> [<?= $level;  ?>]</td>
                            </tr>
                            <tr>
                                <td><a href="profil-petani&id=<?= $id  ?>"><i class="fa fa-user"></i> Profil</a></td>
                            </tr>
                            <tr>
                                <td><a href="keluar.php"><i class="fa fa-sign-out-alt"></i> Keluar</a></td>
                            </tr>
                        </table>
                    </div>
            </div>
        </div>
    </div>
    <!-- End My Account -->

    <!-- toko -->
    <div style="margin-top: -100px" class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Toko Saya</h1>
                    <a href="tambah-produk" style="color: white" class="btn hvr-hover">Tambah produk</a>
                    <!-- <a href="pemesanan" style="color: white" class="btn hvr-hover">Pemesanan Konsumen</a> -->
                    </div>
                </div>
                 <div class="col-lg-12">
                    <div class="table-main table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Gambar</th>
                                    <th>Nama produk</th>

                                    <th>Harga</th>
                                    <th>Minimal Pembelian</th>
                                    <th>Stok</th>
                                    <th>Expired</th>
                                    <th>Deskripsi</th>
                                    <th>Hapus</th>
                                    <th>Rubah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    include"database/koneksi.php";
                                    $no=1;
                                    $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_petani='$id' ORDER BY id_produk DESC");
                                    foreach ($produk as $data_produk) {
                                        # code...
                                    

                                 ?>
                                <tr>
                                    <td class="thumbnail-img">
                                        <a href="#">
                                    <img class="img-fluid" src="page/petani/foto-produk/<?= $data_produk['gambar'] ?>" alt="" />
                                    </a>
                                    </td>
                                    <td class="name-pr"><?= $data_produk['nm_produk']  ?></td>
                                
                                    <td class="price-pr">
                                        <p><?= $data_produk['harga_produk']  ?>/ <?= $data_produk['satuan']  ?></p>
                                    </td>
                                   <td><?= $data_produk['min_pemesanan']  ?><?= $data_produk['satuan']  ?></td>
                                   <td><?= $data_produk['stok']  ?><?= $data_produk['satuan']  ?></td>
                                   <td><?= $data_produk['tgl_expired']  ?></td>
                                   <td><?= $data_produk['deskripsi']  ?></td>
                                    <td class="remove-pr"><a href="hapus-produk&id=<?= $data_produk['id_produk'];  ?>"><i class="fas fa-times"></i></a>
                                    </td>
                                    <td class="remove-pr"><a href="edit-produk&id=<?= $data_produk['id_produk']  ?>"><i class="fas fa-edit"></i></a>
                                    </td>
                                </tr>
                            <?php $no++; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>