<?php 
include"database/koneksi.php";
$petani=mysqli_query($koneksi,"SELECT * FROM tb_petani WHERE id_petani='$id'");
$data_petani=mysqli_fetch_array($petani);

$id=$_GET['id'];
$produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk='$id'");
foreach ($produk as $data_produk) {
    # code...
}

 ?>
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Edit produk produk</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Akun Saya</a></li>
                        <li class="breadcrumb-item active">Edit produk</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">   
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="contact-form-right">
                        <div class="alert alert-info alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <h5><i class="icon fas fa-info"></i> Alert!</h5>
                          Hallo <?= $data_petani['nm_petani'] ?>, ada perubahan dengan produkan anda? silahkan klik form untuk merubah.
                        </div>

                        <h2 align="center">Edit produk</h2>
                        <p align="center">Silahkan isi form semua di bawah ini dengan benar.</p>
                            <form  action="page/petani/aksi/edit_produk.php" method="post" enctype="multipart/form-data">
                                <input type="" hidden="" name="id_produk" value="<?= $data_produk['id_produk'];  ?>">
                                <input type="" name="id_petani" hidden="" value="<?= $data_produk['id_petani']  ?>">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input value="<?= $data_produk['nm_produk']  ?>" type="text" class="form-control"  name="nm_produk" placeholder="Nama produk" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <p>Pilih satuan produk. contoh: Kg,gr,kwintal</p>
                                            <select class="form-control" name="satuan">
                                                <option selected="" value="<?= $data_produk['satuan']  ?>"><?= $data_produk['satuan']  ?></option>
                                                <option value="Kg">Kg</option>
                                                <option value="gr">gr</option>
                                                <option value="kwintal">kwintal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <p>Harga produk /satuan</p>
                                            <input value="<?= $data_produk['harga_produk']  ?>" type="text" class="form-control"  name="harga_produk" placeholder="Harga produk. contoh: 12000"  required="">
                                        </div>
                                    </div>
                                     <div class="col-md-12">
                                        <div class="form-group">
                                            <p>Minimal Pemesanan /Satuan.</p>
                                            <input value="<?= $data_produk['min_pemesanan']  ?>" type="text" class="form-control"  name="min_pembelian" placeholder="Berapa Minimal Pemesanan? contoh: 10" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input value="<?= $data_produk['stok'] ?>" type="text" class="form-control"  name="stok" placeholder="Berapa jumlah stok produk yang anda miliki? contoh: 100" required="">
                                        </div>
                                    </div> 
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <p>Tanggal Expired produk</p>
                                            <input value="<?= $data_produk['tgl_expired']  ?>" type="text" class="form-control"  name="tgl_expired" placeholder="Sampai kapan produkan anda akan bertahan? contoh:  2020-10-20" id="tanggal" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                           <textarea class="form-control" required="" name="deskripsi" placeholder="Ceritakan keunggulan produk yang anda miliki!"><?= $data_produk['deskripsi']  ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn hvr-hover disabled" id="submit" type="submit" style="pointer-events: all; cursor: pointer;color: white;">Edit produk</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            
                        </div>
                    </div>
                </div>
            </div>


