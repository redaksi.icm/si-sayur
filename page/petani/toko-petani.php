<?php 
$petani=mysqli_query($koneksi,"SELECT * FROM tb_petani where id_petani='$_GET[id]'");
foreach ($petani as $dt_petani) {
    # code...

$tgl_now=date('Y-m-d');
$produk=mysqli_query($koneksi,"SELECT * FROM tb_produk where id_petani='$dt_petani[id_petani]'");
foreach ($produk as $dt_produk) {
    
}

}

 ?>
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Toko Petani</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"><?= $dt_petani['nm_petani']  ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-sm-12 col-xs-12 shop-content-right">
                    <div style="border:2px solid; border-radius: 10px" align="center">
                        <br>
                        <i class="fa fa-user" style="font-size: 64px"></i>
                        <table style="font-size: 20px">
                            <tr>
                                <td>Nama Petani</td>
                                <td width="20px">:</td>
                                <td><?= $dt_petani['nm_petani']  ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $dt_petani['alamat']  ?></td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon</td>
                                <td>:</td>
                                <td><?= $dt_petani['no_telepon']  ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="right-product-box">
                        <div class="product-categorie-box">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade show active" id="grid-view">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="title-all text-center">
                                                <h1>Produk</h1>
                                                <p>Dapatkan produkan langsung dari para petani.</p>
                                            </div>
                                        </div>
                                        <?php 
                                            $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk where id_petani='$dt_petani[id_petani]'");
                                            foreach ($produk as $dt_produk) {
                                                
                                            
                                         ?>
                                       <div class="col-lg-3 col-md-6 special-grid best-seller">
                                        <div class="products-single fix">
                                            <div class="box-img-hover">
                                            <img height="200px" width="100%"  src="page/petani/foto-produk/<?= $dt_produk['gambar']  ?>"  alt="Image">                                        </div>
                                        <div class="why-text">
                                            <h4><?= $dt_produk['nm_produk']  ?></h4>
                                            <h5> <?= rupiah($dt_produk['harga_produk'])  ?>/<?= $dt_produk['satuan']  ?></h5>
                                            <p>Minimal pembelian: <?= $dt_produk['min_pemesanan']  ?><?= $dt_produk['satuan']  ?></p>
                                            <p>Stok Tersedia: <?= $dt_produk['stok']  ?><?= $dt_produk['satuan']  ?></p>
                                            <p>Tanggal Expired: <?= $dt_produk['tgl_expired']  ?></p>
                                            <p>Petani: <i class="fa fa-user"></i><a href="toko-petani&id=<?= $dt_petani['id_petani']  ?>" style="color: blue"> <?= $dt_petani['nm_petani']  ?></a></p>
                                           <div class="price-box-bar">
                                        <div class="cart-and-bay-btn">
                                        <form action="aksi/tambah_keranjang.php" method="POST">
                                            <input hidden="" type="" value="<?= $id  ?>" name="id_konsumen">
                                            <input hidden="" type="" value="<?= $dt_produk['id_produk']  ?>" name="id_produk">
                                            <input hidden="" type="" value="<?= $dt_produk['min_pemesanan']  ?>" name="jumlah">
                                            <?php if ($level=='konsumen' && $dt_produk['stok'] > 0): ?>
                                                
                                            <button style="color: #ffffff" class="btn hvr-hover" type="submit" data-fancybox-close=""><i class="fa fa-shopping-bag"></i></button>
                                            <?php endif ?>
                                            <?php if ($dt_produk['stok'] > 0): ?>
                                                
                                            <a class="btn hvr-hover" data-fancybox-close="" href="detail-produk&id=<?= $dt_produk['id_produk']  ?>">Pesan Sekarang</a>
                                            <?php endif ?>
                                            <?php if ($dt_produk['stok']==0): ?>
                                                <p align="center" style="color: red">Stok Habis</p>
                                            <?php endif ?>
                                        </form>
                                        </div>
                                    </div>
                                        </div>
                                        </div>
                                    </div> 
                                <?php } ?>
                                     </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->