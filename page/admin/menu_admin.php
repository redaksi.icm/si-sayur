		<li class="nav-item">
            <a href="beranda-admin&id=<?= $id  ?>" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Beranda
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="data-admin" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Data Admin
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="data-petani" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Data Petani
                
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="data-konsumen" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Data Konsumen
                
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="data-produk" class="nav-link">
              <i class="nav-icon fas fa-shopping-bag"></i>
              <p>
                Data Produk
                
              </p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="data-pemesanan" class="nav-link">
              <i class="nav-icon fas fa-shopping-cart"></i>
              <p>
                Data Pemesanan
                
              </p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="data-transaksi" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Data Transaksi
                
              </p>
            </a>
          </li>