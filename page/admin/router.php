<?php 

if(isset($_GET['p'])){
		$page = $_GET['p'];
 
		switch ($page) {
			case 'beranda-admin':
				include "beranda.php";
				break;
			case 'data-admin':
				include "page/data-admin.php";
				break;
			case 'data-petani':
				include "page/data-petani.php";
				break;
			case 'data-konsumen':
				include "page/data-konsumen.php";
				break;
			case 'data-pemesanan':
				include "page/data-pemesanan.php";
				break;	
			case 'data-produk':
				include "page/data-produk.php";
				break;			
			case 'data-transaksi':
				include "page/data-transaksi.php";
				break;
			case 'hapus-admin':
				include "page/aksi/hapus-admin.php";
				break;
			case 'hapus-petani':
				include "page/aksi/hapus-petani.php";
				break;
			case 'hapus-konsumen':
				include "page/aksi/hapus-konsumen.php";
				break;
			case 'hapus-produk':
				include "page/aksi/hapus-produk.php";
				break;
			case 'aktif-akun-petani':
				include "page/aksi/aktif-akun-petani.php";
				break;
			case 'aktif-akun-konsumen':
				include "page/aksi/aktif-akun-konsumen.php";
				break;
			case 'konfirmasi':
				include "page/aksi/konfirmasi-pemesanan.php";
				break;
			case 'konfirmasi-pembayaran':
				include "page/aksi/konfirmasi-pembayaran.php";
				break;
			case 'hapus-pemesanan':
				include "page/aksi/hapus-pemesanan.php";
				break;
			case 'hapus-transaksi':
				include "page/aksi/hapus-transaksi.php";
				break;

	
			
						
			// default:
			// 	include"error.php";
			// 	break;
		}
	}else{

		include"beranda.php";
		
		
	}

 ?>