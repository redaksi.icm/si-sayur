<?php 
if (isset($_GET['berhasil'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Hapus data berhasil!
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<?php 
if (isset($_GET['akun-aktif'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Akun berhasil diaktifkan!
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Petani</h3>

              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Petani</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Jekel</th>
                    <th>Tanggal Bergabung</th>
                    <th>Status</th>
                    <th>Aktifkan Akun</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no=1; 
                    $petani=mysqli_query($koneksi,"SELECT * FROM tb_petani ORDER BY id_petani DESC");
                    foreach ($petani as $data_petani) {
                      # code...
                    

                   ?>
                  <tr>
                    <td><?= $no  ?></td>
                    <td><?= $data_petani['id_petani']  ?></td>
                    <td><?= $data_petani['nm_petani']  ?></td>
                    <td><?= $data_petani['email']  ?></td>
                    <td><?= $data_petani['no_telepon']  ?></td>
                    <td><?= $data_petani['alamat']  ?></td>
                    <td><?= $data_petani['jekel']  ?></td>
                    <td><?= $data_petani['tgl_bergabung']  ?></td>
                    <td>
                      <?php 
                        if ($data_petani['status']=='1') {
                          echo "<button class='btn btn-danger'><i class='fa fa-ban'> Nonaktif</i></button>";
                        }else{
                           echo "<button class='btn btn-success'><i class='fa fa-check'> Aktif</i></button>";
                        }
                       ?>
                    </td>
                    <td>
                      <?php if ($data_petani['status']=='1'): ?>
                        
                      <a href="aktif-akun-petani&id=<?= $data_petani['id_petani']  ?>" class="btn btn-success"><i class="fa fa-check"> Aktfikan</i></a>
                      <?php endif ?>
                    </td>
                    <td>
                      <a href="hapus-petani&id=<?= $data_petani['id_petani']  ?>" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>
                    </td>
                   
                  </tr>
                  <?php $no++; } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>