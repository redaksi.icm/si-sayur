<?php ob_start(); ?>

<?php
include "../../../../database/koneksi.php";
require_once("dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();

$tahun=$_POST['tahun'];
$bulan=$_POST['bulan'];
$date=date('l, d-m-Y');

 if ($bulan=='01') {
       $nm='Januari';
   }elseif ($bulan=='02') {
       $nm='Februari';
   }elseif ($bulan=='03') {
       $nm='Maret';
   }elseif ($bulan=='04') {
       $nm='April';
   }elseif ($bulan=='05') {
       $nm='Mei';
   }elseif ($bulan=='06') {
       $nm='Juni';
   }elseif ($bulan=='07') {
       $nm='Juli';
   }elseif ($bulan=='08') {
       $nm='Agustus';
   }elseif ($bulan=='09') {
       $nm='September';
   }elseif ($bulan=='10') {
       $nm='Oktober';
   }elseif ($bulan=='11') {
       $nm='November';
   }elseif ($bulan=='12') {
       $nm='Desember';
   }

$html ='<table width="100%" style="font-family: sans-serif; background-color: #00BFFF; padding: 10px">
    <tr>
        <td><p style="font-size: 36px"><b>PasaSayua</b></p> <br>
            <p style="margin-top: -60px">Kampung Batu Dalam</p>
        </td>
    </tr>
</table>
<br><br>
<table width="100%" align="center" style="font-family: sans-serif;">
    <tr>
        <td align="center">
            <p style="font-size: 24px"><b>Laporan Transaksi Penjualan PasaSayua <br> Pada '.$nm.' Tahun '.$tahun.'</b></p>
            <p>Tanggal Cetak: '.$date.'</p>
        </td>
    </tr>
</table>
<br><br>';

$html .='<table border="3" width="100%" style="font-family: sans-serif;border-collapse: collapse;border-color: #eac228">
    <tr>
         <th>No</th>
         <th>ID Pemesanan</th>
         <th>Tanggal</th>
         <th>Nama Konsumen</th>
         <th>Total Pembayaran</th>
         <th>Status</th>
    </tr>';
    function rupiah($angka) {
              $hasil_rupiah= "Rp " . number_format($angka,2,',','.');
              return $hasil_rupiah;
        }

    $no=1;
    $transaksi=mysqli_query($koneksi,"SELECT sum(total_harga) as total,id_pemesanan,tgl_pemesanan,status,id_produk,id_konsumen FROM tb_pemesanan WHERE YEAR(tgl_pemesanan)=$tahun AND MONTH(tgl_pemesanan)=$bulan AND status=4  GROUP BY id_pemesanan ORDER BY tgl_pemesanan DESC");
                    foreach ($transaksi as $dt_transaksi) {
                     
                     $konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen WHERE id_konsumen='$dt_transaksi[id_konsumen]'");
                     foreach ($konsumen as $dt_konsumen) {
                       # code...
                     }
                 if ($dt_transaksi['status']=='4') {
                     $status='Lunas';
                 }

$html .='<tr style="background-color: #bbbbbb">
        <td>'.$no.'</td>
        <td>'.$dt_transaksi['id_pemesanan'].'</td>
        <td>'.$dt_transaksi['tgl_pemesanan'].'</td>
        <td>'.$dt_konsumen['nm_konsumen'].'</td>
        <td>'.rupiah($dt_transaksi['total']).'</td>
        <td>'.$status.'</td>
    </tr>';
$no++;

}

$html .='</table>';
$html .='<br><br>';

$total=mysqli_query($koneksi,"SELECT sum(total_harga) as grand_total FROM tb_pemesanan WHERE YEAR(tgl_pemesanan)= $tahun AND MONTH(tgl_pemesanan)=$bulan AND status='4'");
foreach ($total as $dt_total) {
    # code...
}
$pesan=mysqli_query($koneksi,"SELECT count(id) as id_pesan FROM tb_pemesanan WHERE YEAR(tgl_pemesanan)=$tahun AND MONTH(tgl_pemesanan)=$bulan AND status='4' GROUP BY id_pemesanan");
foreach ($pesan as $dt_pesan) {
    # code...
}
$html .='<div style="border: 2px solid #eac228; width: 80%;padding: 10px; font-weight: 100%; border-radius: 10px">
    <table  width="50%" style="font-family: sans-serif;border-collapse: collapse;">
        <tr style="height: 50px">
             <td><b>Total Pendapatan</b></td>
             <td>:</td>
             <td>'.rupiah($dt_total['grand_total']).'</td>
        </tr>
        
    </table>
</div>';



 
  



$date=date('l, d-m-Y');
$html .= "</html>";
$dompdf->loadHtml($html);
// Setting ukuran dan orientasi kertas
$dompdf->setPaper('A4', 'landscape');
// Rendering dari HTML Ke PDF
$dompdf->render();
// Melakukan output file Pdf
$dompdf->stream('laporan-transaksi.pdf');
?>