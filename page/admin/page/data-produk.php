<?php 
if (isset($_GET['konfirmasi'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Pemesanan berhasil di konfirmasi
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<?php 
if (isset($_GET['berhasil'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Produk berhasil dihapus
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Produk</h3>

              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Nama produk</th>
                    <th>Nama Petani</th>
                   
                    <th>Harga produk</th>
                    <th>Minimal Pemesanan</th>
                    <th>Stok</th>
                    <th>Tanggal Upload</th>
                    <th>Tanggal Expired</th>
                    <th>Deskripsi</th>
                    <th>Hapus</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no=1; 
                    $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk ORDER BY tgl_upload DESC");
                    foreach ($produk as $data_produk) {

                    $petani=mysqli_query($koneksi,"SELECT * FROM tb_petani WHERE id_petani='$data_produk[id_petani]'");
                    foreach ($petani as $dt_petani) {
                    	# code...
                    }
                    	
                    

                   ?>
                  <tr>
                  	<td><?= $no ?></td>
                  	<td>
                  		<a href="../petani/foto-produk/<?= $data_produk['gambar'] ?>">
                  			<img width="100%" src="../petani/foto-produk/<?= $data_produk['gambar'] ?>">
                  		</a>
                  	</td>
                  	<td><?= $data_produk['nm_produk']  ?></td>
                  	<td><?= $dt_petani['nm_petani']  ?></td>
                  	<td><?= rupiah($data_produk['harga_produk'])  ?>/<?= $data_produk['satuan']  ?></td>
                  	<td><?= $data_produk['min_pemesanan']  ?>/<?= $data_produk['satuan']  ?></td>
                  	<td><?= $data_produk['stok']  ?><?= $data_produk['satuan']  ?></td>
                  	<td><?= $data_produk['tgl_upload']  ?></td>
                  	<td><?= $data_produk['tgl_expired']  ?></td>
                  	<td><?= $data_produk['deskripsi']  ?></td>
                  	<td>
                  		<a href="hapus-produk&id=<?= $data_produk['id_produk']  ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</a>
                  	</td>
                  </tr>
                  <?php $no++; } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>