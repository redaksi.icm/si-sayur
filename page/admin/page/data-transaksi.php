<?php 
if (isset($_GET['konfirmasi'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Transaksi berhasil di konfirmasi
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<?php 
if (isset($_GET['berhasil-dihapus'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Transaksi berhasil dihapus
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Transaksi</h3>

              </div> 
              <div class="card-header">
                <div class="col-lg-4">
                <h3 class="card-title">Buat Laporan</h3><br><br>
                <form action="page/aksi/print-laporan-transaksi.php" method="POST">
                <div class="form-group">
                  <p>Pilih Tahun</p>
                  <select class="form-control" name="tahun">
                    <?php 
                    $tahun=date('Y');
                      for ($i=2017; $i <= $tahun ; $i++) {   
                     ?>
                    <option value="<?= $i;  ?>"><?= $i;  ?></option>
                  <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <p>Pilih Bulan</p>
                  <select class="form-control" name="bulan">
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div> 
                <div class="form-group">
                  <button type="submit" class="btn btn-danger"><i class="fa fa-print"></i> Print</button>
                </div>
              </div>
            </form>

              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Pemesanan</th>
                    <th>Tanggal</th>
                    <th>Nama Konsumen</th>
                    <th>Total Pembayaran</th>
                    <th>Status</th>
                    <th>Hapus</th>
                   
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no=1; 
                    $transaksi=mysqli_query($koneksi,"SELECT sum(total_harga) as total,id_pemesanan,tgl_pemesanan,status,id_produk,id_konsumen FROM tb_pemesanan  GROUP BY id_pemesanan ORDER BY tgl_pemesanan DESC");
                    foreach ($transaksi as $dt_transaksi) {
                     
                     $konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen WHERE id_konsumen='$dt_transaksi[id_konsumen]'");
                     foreach ($konsumen as $dt_konsumen) {
                       # code...
                     }
                    
                    

                   ?>
                  <tr>
                    <td><?= $no  ?></td>
                    <td><?= $dt_transaksi['id_pemesanan']  ?></td>
                    <td><?= $dt_transaksi['tgl_pemesanan']  ?></td>
                    <td><?= $dt_konsumen['nm_konsumen']  ?></td>
                    <td><?= rupiah($dt_transaksi['total'])  ?></td>
                    <td>
                      <?php if ($dt_transaksi['status']==4): ?>
                        <button class="btn btn-success"><i class="fa fa-check"></i>Berhasil</button>
                      <?php endif ?>
                    </td>
                    <td>
                      <a href="hapus-transaksi&id=<?= $dt_transaksi['id_pemesanan']  ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</a>
                    </td>
                  	
                  </tr>
                    <?php $no++;  } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>