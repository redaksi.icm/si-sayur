<?php 
if (isset($_GET['berhasil'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Hapus data berhasil!
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Admin</h3>

              </div>
              <div class="card-header">
                <a style="color: #ffffff" data-toggle="modal" data-target="#tambah-admin" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data Admin</a>
                <?php include"page/form-tambah-admin.php" ?>
               
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Admin</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Jekel</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no=1; 
                    $admin=mysqli_query($koneksi,"SELECT * FROM tb_admin ORDER BY id_admin DESC");
                    foreach ($admin as $data_admin) {
                      # code...
                    

                   ?>
                  <tr>
                    <td><?= $no  ?></td>
                    <td><?= $data_admin['id_admin']  ?></td>
                    <td><?= $data_admin['nm_admin']  ?></td>
                    <td><?= $data_admin['email']  ?></td>
                    <td><?= $data_admin['no_telepon']  ?></td>
                    <td><?= $data_admin['alamat']  ?></td>
                    <td><?= $data_admin['jekel']  ?></td>
                    <td>
                      <a href="hapus-admin&id=<?= $data_admin['id_admin']  ?>" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>
                    </td>
                   
                  </tr>
                  <?php $no++; } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>