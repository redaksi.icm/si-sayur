<?php 
if (isset($_GET['konfirmasi'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Pemesanan berhasil di konfirmasi
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<?php 
if (isset($_GET['konfirmasi-pembayaran'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Pembayaran telah berhasil
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<?php 
if (isset($_GET['berhasil-dihapus'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Pemesanan berhasil dihapus
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data pemesanan</h3>

              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Pemesanan</th>
                    <th>Nama Konsumen</th>
                    <th>Detail Pemesanan</th>
                    <th>Total Harga</th>
                    <th>Tanggal Pemesanan</th>
                    <th>Status</th>
                    <th>Konfirmasi</th>
                    <th>Hapus</th>
                    <th>Pembayaran</th>
                    <th>Konfirmasi Pembayaran</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no=1; 
                    $pemesanan=mysqli_query($koneksi,"SELECT sum(total_harga) as total,id_pemesanan,tgl_pemesanan,status,id_produk,id_konsumen FROM tb_pemesanan  GROUP BY id_pemesanan ORDER BY id_pemesanan DESC");
                    foreach ($pemesanan as $data_pemesanan) {
                      # code...

                      $konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen WHERE id_konsumen='$data_pemesanan[id_konsumen]'");
                     foreach ($konsumen as $dt_konsumen) {
                       # code...
                     }
                    
                    

                   ?>
                  <tr>
                  	<td><?= $no  ?></td>
                    <td><?= $data_pemesanan['id_pemesanan'] ?></td>
                    <td><?= $dt_konsumen['nm_konsumen']  ?></td>
                    <td>
                      <?php 
                        $detail=mysqli_query($koneksi,"SELECT * FROM tb_pemesanan WHERE id_pemesanan='$data_pemesanan[id_pemesanan]'");

                          foreach ($detail as $data_detail) {
                          $pemesanan=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk='$data_detail[id_produk]'");


                            foreach ($pemesanan as $dt_pemesanan) {
                              }
                                  echo $dt_pemesanan['nm_produk']." x ".$data_detail['jumlah_pemesanan']."/".$dt_pemesanan['satuan']."<br>";

                               }
                               ?>
                                    </td>
                                    <td><?= rupiah($data_pemesanan['total']) ?></td>
                                    <td><?= $data_pemesanan['tgl_pemesanan'] ?></td>
                                    <td>
                                        <?php 
                                            if ($data_pemesanan['status']=='1') {
                                                echo "<p style='color:red'>Ditunda<b>";
                                            }elseif($data_pemesanan['status']=='2'){
                                                echo "<p style='color:red;'>Menunggu Pembayaran</p>";
                                            }elseif ($data_pemesanan['status']=='3') {
                                                echo "<p style='color:yellow'>Pembayaran dalam pengecekan</p>";
                                            }elseif ($data_pemesanan['status']=='4') {
                                                
                                                echo "<button class='btn btn-success'>Sudah di bayar</button>";
                                            
                                            }
                                         ?>
                                    </td>
                                    <td>
    								      <?php if ($data_pemesanan['status']==1): ?>
    									
                                    	<a href="konfirmasi&id=<?= $data_pemesanan['id_pemesanan']  ?>" class="btn btn-success"><i class="fa fa-check"></i> Konfirmasi</a></td>
    								      <?php endif ?>

                                    <td><a href="hapus-pemesanan&id=<?= $data_pemesanan['id_pemesanan']  ?>" class="btn btn-danger"><i class="fa fa-trash-alt"></i> Hapus</a></td>

                                    <td><a href="" data-toggle="modal" data-target="#lihat-pembayaran<?= $data_pemesanan['id_pemesanan']  ?>" class="btn btn-warning"><i class="fa fa-eye"></i> Lihat</a></td>

                                    <?php include"lihat-pembayaran.php" ?>

                                    <td><a class="btn btn-primary" href="konfirmasi-pembayaran&id=<?= $data_pemesanan['id_pemesanan']  ?>"><i class="fa fa-check"></i> Konfirmasi Pembayaran</a></td>
                  </tr>
                  <?php $no++; } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>