<?php 
if (isset($_GET['berhasil'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Hapus data berhasil!
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>

<?php 
if (isset($_GET['akun-aktif'])) {
  


 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
            Akun berhasil diaktifkan!
        </div> 
      </div>
    </div>
  </div>
</section>   
<?php } ?>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
             <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data konsumen</h3>

              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>ID konsumen</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Telepon</th>
                    <th>Alamat</th>
                    <th>Jekel</th>
                    <th>Tanggal Bergabung</th>
                    <th>Status</th>
                    <th>Aktifkan Akun</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $no=1; 
                    $konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen ORDER BY id_konsumen DESC");
                    foreach ($konsumen as $data_konsumen) {
                      # code...
                    

                   ?>
                  <tr>
                    <td><?= $no  ?></td>
                    <td><?= $data_konsumen['id_konsumen']  ?></td>
                    <td><?= $data_konsumen['nm_konsumen']  ?></td>
                    <td><?= $data_konsumen['email']  ?></td>
                    <td><?= $data_konsumen['no_telepon']  ?></td>
                    <td><?= $data_konsumen['alamat']  ?></td>
                    <td><?= $data_konsumen['jekel']  ?></td>
                    <td><?= $data_konsumen['tgl_bergabung']  ?></td>
                    <td>
                      <?php 
                        if ($data_konsumen['status']=='1') {
                          echo "<button class='btn btn-danger'><i class='fa fa-ban'> Nonaktif</i></button>";
                        }else{
                           echo "<button class='btn btn-success'><i class='fa fa-check'> Aktif</i></button>";
                        }
                       ?>
                    </td>
                    <td>
                      <?php if ($data_konsumen['status']=='1'): ?>
                        
                      <a href="aktif-akun-konsumen&id=<?= $data_konsumen['id_konsumen']  ?>" class="btn btn-success"><i class="fa fa-check"> Aktfikan</i></a>
                      <?php endif ?>
                    </td>
                    <td>
                      <a href="hapus-konsumen&id=<?= $data_konsumen['id_konsumen']  ?>" class="btn btn-danger"><i class="fa fa-trash-alt"></i></a>
                    </td>
                   
                  </tr>
                  <?php $no++; } ?>
                  </tbody>
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>