<?php 
$petani=mysqli_query($koneksi,"SELECT count(status) as hitung_status FROM tb_petani WHERE status='1'");
$data_petani=mysqli_fetch_array($petani);
$hitung_petani=$data_petani['hitung_status'];



$konsumen=mysqli_query($koneksi,"SELECT count(status) as hitung_status FROM tb_konsumen WHERE status='1'");
$data_konsumen=mysqli_fetch_array($konsumen);
$hitung_konsumen=$data_konsumen['hitung_status'];


$pemesanan=mysqli_query($koneksi,"SELECT count(id_pemesanan) as hitung_status FROM tb_pemesanan WHERE status='1'");
$data_pemesanan=mysqli_fetch_array($pemesanan);
$hitung_pemesanan=$data_pemesanan['hitung_status'];


$pembayaran=mysqli_query($koneksi,"SELECT count(status) as hitung_status FROM tb_pemesanan WHERE status='3'");
$data_pembayaran=mysqli_fetch_array($pembayaran);
$hitung_pembayaran=$data_pembayaran['hitung_status'];

 ?>
<section class="content">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <?php if ($hitung_petani > 0): ?>
          
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Pemberitahuan!</h5>
                        Terdapat <?= $hitung_petani  ?> Petani yang baru mendaftar <a href="data-petani">lihat</a>
        </div> 
        <?php endif ?>
        <?php if ($hitung_konsumen > 0): ?>
          
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Pemberitahuan!</h5>
                        Terdapat <?= $hitung_konsumen  ?> Konsumen yang baru mendaftar <a href="data-konsumen">lihat</a>
        </div> 
        <?php endif ?>
        <?php if ($hitung_pemesanan > 0): ?>
          
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Pemberitahuan!</h5>
                        Terdapat <?= $hitung_pemesanan  ?> Pemesanan baru! <a href="data-pemesanan">lihat</a>
        </div>
        <?php endif ?>

         <?php if ($hitung_pembayaran > 0): ?>
          
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Pemberitahuan!</h5>
                        Terdapat <?= $hitung_pembayaran  ?> Pembayaran baru! <a href="data-pemesanan">lihat</a>
        </div>
        <?php endif ?>
         <?php if (isset($_GET['rubah-profil'])): ?>
          
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-info"></i> Alert!</h5>
                Profil Berhasil di Rubah
        </div>
        <?php endif ?>
       
      </div>
    </div>
  </div>
</section>   
<section class="content">
      <div class="container-fluid">
      <div class="row">
<?php 
$pemesanan=mysqli_query($koneksi,"SELECT count(id_admin) as hitung_admin FROM tb_admin ");
$data_pesan=mysqli_fetch_array($pemesanan);
 ?>
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?= $data_pesan['hitung_admin']  ?></h3>

                <p>Admin</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              <a href="data-admin" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
<?php 
$produk=mysqli_query($koneksi,"SELECT count(id_produk) as hitung_produk FROM tb_produk ");
$data_produk=mysqli_fetch_array($produk);
 ?>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?= $data_produk['hitung_produk']  ?></h3>

                <p>Produk</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="data-produk" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
<?php 
$konsumen=mysqli_query($koneksi,"SELECT count(id_konsumen) as hitung_konsumen FROM tb_konsumen ");
$data_konsumen=mysqli_fetch_array($konsumen);
 ?>
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3><?= $data_konsumen['hitung_konsumen']  ?></h3>

                <p>Konsumen</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
              <a href="data-konsumen" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
<?php 
$petani=mysqli_query($koneksi,"SELECT count(id_petani) as hitung_petani FROM tb_petani ");
$data_petani=mysqli_fetch_array($petani);
 ?>
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><?= $data_petani['hitung_petani']  ?></h3>

                <p>Petani</p>
              </div>
              <div class="icon">
                <i class="fas fa-user"></i>
              </div>
              <a href="data-petani" class="small-box-footer">
                More info <i class="fas fa-arrow-circle-right"></i>
              </a>
            </div>
          </div>
          <!-- ./col -->
        </div>
      </div>
    </section>