    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Upload Pembayaran</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="pemesanan-konsumen">Pemesanan</a></li>
                        <li class="breadcrumb-item active">Upload Pembayaran</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- form -->
    <div class="contact-box-main" >
        <div class="container">
            <div class="row">
                <div class="col-lg-3">   
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="contact-form-right">
                        <h2 align="center">Upload Pembayaran</h2>
                        <p align="center">Pastikan jumlah pembayaran sama dengan total harga yang dipesan.</p>
                            <form  action="page/konsumen/aksi/upload_pembayaran.php" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>ID Pemesanan</p>
                                        <input type="" readonly="" class="form-control" name="id_pemesanan" value="<?= $_GET['id'];  ?>"><br>
                                        <p>Jumlah Pembayaran</p>
                                        <input placeholder="Jumlah Pembayaran" type="" name="jumlah_bayar" class="form-control">
                                        <br>
                                        <p>Upload Bukti</p>
                                        <input type="file" name="file" class="form-control"><br>
                                    </div>

                                    <div class="col-md-12">
                                        <button class="btn hvr-hover disabled" id="submit" type="submit" style="pointer-events: all; cursor: pointer;color: white;">Upload</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">  
                        </div>
                    </div>
                </div>
            </div>