<?php 
include"database/koneksi.php";
$konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen WHERE id_konsumen='$id'");
$data_konsumen=mysqli_fetch_array($konsumen);

 ?>
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Akun Saya</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Akun Saya</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start My Account  -->
    <div class="my-account-box-main">
        <div class="container">
            <div class="my-account-page">
                    <div style="border:2px solid;border-radius:10px;padding: 20px;" align="center">
                        <i class="fa fa-user" style="font-size: 48px"></i>
                        <table style="text-align: center;">
                            <tr>
                                <td style="font-size: 20px"><?= $data_konsumen['nm_konsumen'];  ?> [<?= $level;  ?>]</td>
                            </tr>
                            <tr>
                                <td><a href="keluar.php"><i class="fa fa-sign-out-alt"></i> Keluar</a></td>
                            </tr>
                        </table>
                    </div>
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="account-box">
                            <div class="service-box">
                                <div class="service-icon">
                                    <a href="profil-konsumen&id=<?= $data_konsumen['id_konsumen']  ?>"> <i class="fa fa-user"></i> </a>
                                </div>
                                <div class="service-desc">
                                    <h4>Profil</h4>
                                    <p>Edit nama,nomor telepon dll</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="account-box">
                            <div class="service-box">
                                <div class="service-icon">
                                    <a href="pemesanan-konsumen"> <i class="fa fa-gift"></i> </a>
                                </div>
                                <div class="service-desc">
                                    <h4>Pesanan</h4>
                                    <p>Pantau status pesananmu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="account-box">
                            <div class="service-box">
                                <div class="service-icon">
                                    <a href="riwayat-pemesanan&id=<?= $id  ?>"> <i class="fa fa-list"></i> </a>
                                </div>
                                <div class="service-desc">
                                    <h4>Riwayat Pemesanan</h4>
                                    <p>Pesananmu tersimpan disini.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End My Account -->