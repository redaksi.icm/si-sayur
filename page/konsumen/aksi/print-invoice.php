<?php ob_start(); ?>

<?php
include "../../../database/koneksi.php";
require_once("dompdf/autoload.inc.php");
use Dompdf\Dompdf;
$dompdf = new Dompdf();

$id_pemesanan=$_POST['id'];
$invoice=mysqli_query($koneksi,"SELECT * FROM tb_pemesanan WHERE id_pemesanan='$id_pemesanan' GROUP BY id_pemesanan");
foreach ($invoice as $dt_invoice) {
   $id_produk=$dt_invoice['id_produk'];
   $id_konsumen=$dt_invoice['id_konsumen'];
   $tgl_pemesanan=$dt_invoice['tgl_pemesanan'];
 

   if ($dt_invoice['status']=='1') {
       $status="Ditunda";
   }elseif ($dt_invoice['status']=='2') {
       $status="Menunggu Pembayaran";
   }elseif ($dt_invoice['status']=='3') {
       $status="Pembayaran dalam pengecekan";
   }elseif ($dt_invoice['status']=='4') {
       $status="Lunas";
   }


}
$konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen WHERE id_konsumen='$dt_invoice[id_konsumen]'");
foreach ($konsumen as $dt_konsumen) {
    $nama_konsumen=$dt_konsumen['nm_konsumen'];
    $nomor=$dt_konsumen['no_telepon'];
    $alamat=$dt_konsumen['alamat'];
    $email=$dt_konsumen['email'];
}
 
$html ='<table style="border-collapse: collapse;background-color:#00BFFF;padding:10px;"  width="100%">    
        <tr>
            <td style="font-size: 24px">INVOICE <br><br></td>
            <td colspan="2" style="font-size: 24px"><br><br></td>
            <td style="font-size: 36px">PasaSayua<br><p style="font-size: 16px; margin-top: -5px">Kampung Batu Dalam</p><br></td>
            
        </tr>
        <tr>
            <td>ID Pemesanan</td>
            <td>: </td>
            <td>'.$id_pemesanan.'</td>
        </tr>
        <tr>
            <td>Nama Konsumen</td>
            <td>: </td>
            <td>'.$nama_konsumen.'</td>
        </tr>
        <tr>
            <td>Nomor Telepon</td>
            <td>: </td>
            <td>'.$nomor.'</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>: </td>
            <td>'.$email.'</td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>: </td>
            <td>'.$alamat.'</td>
        </tr>
        <tr>
            <td>Status</td>
            <td>: </td>
            <td>'.$status.'</td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>: </td>
            <td>'.$tgl_pemesanan.'</td>
        </tr>
</table>
<br><br>
<table border="2" style="width: 100%; border-collapse: collapse;font-family: sans-serif;">  
    <tr style="height: 50px; text-align: center;background-color: #eac228">
        <td>Detail</td>
        <td>Jumlah</td>
        <td>Harga</td>
        <td>Sub Total</td>
    </tr>';
        function rupiah($angka) {
              $hasil_rupiah= "Rp " . number_format($angka,2,',','.');
              return $hasil_rupiah;
        }
    $detail_pemesanan=mysqli_query($koneksi,"SELECT * FROM tb_pemesanan WHERE id_pemesanan='$id_pemesanan'");
    foreach ($detail_pemesanan as $dt_detail) {
        $jumlah=$dt_detail['jumlah_pemesanan'];
        $sub_total=$dt_detail['total_harga'];
    
    
    $detail_produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk='$dt_detail[id_produk]'");
    foreach ($detail_produk as $dt_produk) {
    $nm_produk=$dt_produk['nm_produk'];
    $harga=$dt_produk['harga_produk'];
    $satuan=$dt_produk['satuan'];

}

   $html .=' <tr style="height: 50px;">
                <td>'.$nm_produk.'</td>
                <td>'.$jumlah.'/'.$satuan.'</td>
                <td>'.rupiah($harga).'</td>
                <td>'.rupiah($sub_total).'</td>
            </tr>';

}

$html .='</table>';

$html .='<br><br>';
$grand_total=mysqli_query($koneksi,"SELECT sum(total_harga) as total FROM tb_pemesanan WHERE id_pemesanan='$id_pemesanan'");
foreach ($grand_total as $dt_grand) {
    $total=$dt_grand['total'];
}
$pembayaran=mysqli_query($koneksi,"SELECT * FROM tb_pembayaran WHERE status = 2 AND  id_pemesanan='$id_pemesanan'");
foreach ($pembayaran as $dt_pembayaran) {
}
if (empty($dt_pembayaran)) {
    $pem=0;
}else{
    $pem=$dt_pembayaran['jumlah_bayar'];
}

$sisa= $total - $pem;
$html .='<table align="right" style="width: 40%; border-collapse: collapse;">    
    <tr>
        <td>Grand Total</td>
        <td>:</td>
        <td>'.rupiah($total).'</td>
    </tr>
    <tr>
        <td>Jumlah Pembayaran</td>
        <td>:</td>
        <td>'.rupiah($pem).'</td>
    </tr>
    <tr>
        <td>Sisa</td>
        <td>:</td>
        <td>'.rupiah($sisa).'</td>
    </tr>
</table>
<br><br><br>    
<hr align="right" width="40%">';

  



$date=date('l, d-m-Y');
$html .= "</html>";
$dompdf->loadHtml($html);
// Setting ukuran dan orientasi kertas
$dompdf->setPaper('A4', 'Portrait');
// Rendering dari HTML Ke PDF
$dompdf->render();
// Melakukan output file Pdf
$dompdf->stream('invoice-pemesanan.pdf');
?>