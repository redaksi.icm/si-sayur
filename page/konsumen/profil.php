<?php 
$id=$_GET['id'];
$konsumen=mysqli_query($koneksi,"SELECT * FROM tb_konsumen WHERE id_konsumen='$id'");
foreach ($konsumen as $data_konsumen) {
    
}

 ?>
    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Profil konsumen</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="konsumen&id=<?= $id ?>">Akun konsumen</a></li>
                        <li class="breadcrumb-item active"><?= $data_konsumen['nm_konsumen']  ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-sm-12 col-xs-12 shop-content-right">
                    <div style="border:2px solid; border-radius: 10px" align="center">
                        <br>
                        <i class="fa fa-user" style="font-size: 64px"></i>
                        <table style="font-size: 20px">
                            <tr>
                                <td>Nama konsumen</td>
                                <td width="20px">:</td>
                                <td><?= $data_konsumen['nm_konsumen']  ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td width="20px">:</td>
                                <td><?= $data_konsumen['email']  ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                               <td><?= $data_konsumen['alamat']  ?></td>
                            </tr>
                            <tr>
                                <td>Nomor Telepon</td>
                                <td>:</td>
                                <td><?= $data_konsumen['no_telepon']  ?></td>
                            </tr>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    
    <!-- End Shop Page -->

    <div class="contact-box-main" style="margin-top: -150px">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">   
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="contact-form-right">
                        <h2 align="center">Edit Profil</h2>
                        <p align="center">Silahkan isi form semua di bawah ini dengan benar.</p>
                            <form  action="page/konsumen/aksi/rubah_profile.php" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="" name="id_konsumen" hidden="" value="<?= $data_konsumen['id_konsumen']  ?>">
                                            <input value="<?= $data_konsumen['nm_konsumen']  ?>" type="text" class="form-control"  name="nm_konsumen" placeholder="Nama konsumen" required="">
                                        </div>
                                        <div class="form-group">
                                            <input value="<?= $data_konsumen['email']  ?>" type="text" class="form-control"  name="email" placeholder="Nama konsumen" required="">
                                        </div>
                                        <div class="form-group">
                                            <input value="<?= $data_konsumen['no_telepon']  ?>" type="text" class="form-control"  name="no_telepon" placeholder="Nama konsumen" required="">
                                        </div>
                                        <div class="form-group">
                                            <input value="<?= $data_konsumen['alamat']  ?>" type="text" class="form-control"  name="alamat" placeholder="Nama konsumen" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        <button class="btn hvr-hover disabled" id="submit" type="submit" style="pointer-events: all; cursor: pointer;color: white;">Edit Profil</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">  
                        </div>
                    </div>
                </div>
            </div>

<!-- rubah password -->
<?php 
$user=mysqli_query($koneksi,"SELECT * FROM tb_login where id_user='$data_konsumen[id_konsumen]'");
foreach ($user as $data_user) {
    
}

 ?>
    <div class="contact-box-main" style="margin-top: -150px">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">   
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="contact-form-right">
                        <h2 align="center">Edit Password</h2>
                        <p align="center">Silahkan isi form semua di bawah ini dengan benar.</p>
                            <form  action="page/konsumen/aksi/rubah-password.php" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="" hidden="" name="id_user" value="<?= $data_user['id_user'];  ?>">
                                        <p>Password Sekarang</p>
                                        <div class="form-group">
                                            <input value="<?= $data_user['password']  ?>"  type="text" class="form-control"  name="pass_now" placeholder="Password Sekarang" required="">
                                        </div>
                                        <p>Password Baru</p>
                                        <div class="form-group">
                                            <input  type="text" class="form-control"  name="pass_baru" placeholder="Masukan Password Baru" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn hvr-hover disabled" id="submit" type="submit" style="pointer-events: all; cursor: pointer;color: white;">Edit Password</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">  
                        </div>
                    </div>
                </div>
            </div>

<!-- rubah password -->