     <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Pemesanan</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="konsumen&id=<?= $id  ?>">Akun Saya</a></li>
                        <li class="breadcrumb-item active">Pemesanan</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

 <!-- pemesanan -->
 <div  class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Pemesanan</h1>
                    </div>
                </div>
                 <div class="col-lg-12">
                    <div class="table-main table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID Pemesanan</th>
                                    <th>Detail Pemesanan</th>
                                    <th>Total Harga</th>
                                    <th>Tanggal Pemesanan</th>
                                    <th>Status</th>
                                    <th>Upload Pembayaran</th>
                                    <th>Print Invoice</th>
                               
                                    <th>Batal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    include"database/koneksi.php";
                                    $no=1;
                                    $produk=mysqli_query($koneksi,"SELECT sum(total_harga) as total,id_pemesanan,tgl_pemesanan,status,id_produk FROM tb_pemesanan WHERE id_konsumen='$id' AND status > 0 GROUP BY id_pemesanan");
                                    foreach ($produk as $data_produk) {
                                    
                              
   ?>
                                <tr>
                                    <td><?= $data_produk['id_pemesanan'] ?></td>
                                    <td>
                                        <?php 
                                            $detail=mysqli_query($koneksi,"SELECT * FROM tb_pemesanan WHERE id_pemesanan='$data_produk[id_pemesanan]'");
                                            foreach ($detail as $data_detail) {
                                            $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk=$data_detail[id_produk]");
                                            foreach ($produk as $dt_produk) {
                                                
                                            }
                                                echo $dt_produk['nm_produk']." x ".$data_detail['jumlah_pemesanan']."/".$dt_produk['satuan']."<br>";
                                            }
                                         ?>
                                    </td>
                                    <td><?= rupiah($data_produk['total']) ?></td>
                                    <td><?= $data_produk['tgl_pemesanan'] ?></td>
                                    <td>
                                        <?php 
                                            if ($data_produk['status']=='1') {
                                                echo "<p style='color:red'>Ditunda<b>";
                                            }elseif($data_produk['status']=='2'){
                                                echo "<p style='color:red;'>Menunggu Pembayaran</p>";
                                            }elseif ($data_produk['status']=='3') {
                                                echo "<p style='color:yellow'>Pembayaran dalam pengecekan</p>";
                                            }elseif ($data_produk['status']=='4') {
                                                
                                                echo "<p style:'color:green;'>Sudah di bayar</p>";
                                            
                                            }
                                         ?>
                                    </td>
                                    <td>
                                        <?php if ($data_produk['status'] > 1): ?>
                                            
                                        <a  href="upload-pembayaran&id=<?= $data_produk['id_pemesanan']  ?>" class="btn hvr-hover" style="color: #ffffff">Upload Pembayaran</a>
                                        <?php endif ?>
                                    </td>
                                    <form action="page/konsumen/aksi/print-invoice.php" method="POST">
                                    <td>
                                        <input hidden="" type="text" name="id" value="<?= $data_produk['id_pemesanan']  ?>"  >
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-print"> </i></button>
                                    </td>
                                    </form>
                                    
                                    <td><a onclick="return confirm('Yakin untuk membatalkan pemesanan?')" class="btn btn-danger" href="hapus-pesanan&id=<?= $data_produk['id_pemesanan']; ?>">Batal</a></td>
                                </tr>
                            <?php $no++; } ?>
                            </tbody>
                        </table>
                            <?php if (empty($data_produk)): ?>
                                <h3 align="center">Tidak ada pemesanan</h3>
                            <?php endif ?>
                    </div>
                </div>
                <div class="col-lg-12">
                    <br><br>
                    <p>Petujuk:</p><br>
                    <p>1. Setelah melakukan pemesanan konsumen menginputkan pembayaran</p><br>
                    <p>2. Konsumen mencetak bukti pemesanan atau invoice sebagai bukti dalam penjemputan produk yang dipesan ke petani</p><br>
                  
                    <p>3. Setelah konsumen melakukan pembayaran , pembatalan tidak bisa dilakukan  </b></p><br>
                    <p>4. Batas pembayaran 1 Jam setelah pemesanan di setujui  </b></p><br>

                </div>
            </div>
        </div>
    </div>



    <br><br><br>
    <br><br><br>
    <br><br><br>