-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Okt 2020 pada 08.23
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pasa_sayua`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` varchar(20) NOT NULL,
  `nm_admin` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `jekel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `nm_admin`, `email`, `no_telepon`, `alamat`, `jekel`) VALUES
('1409369599', 'budi', 'budibudi@gmail.com', '99756865756', 'solok padang', 'Laki-Laki');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_konsumen`
--

CREATE TABLE `tb_konsumen` (
  `id_konsumen` varchar(20) NOT NULL,
  `nm_konsumen` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `jekel` varchar(20) NOT NULL,
  `tgl_bergabung` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_konsumen`
--

INSERT INTO `tb_konsumen` (`id_konsumen`, `nm_konsumen`, `email`, `no_telepon`, `alamat`, `jekel`, `tgl_bergabung`, `status`) VALUES
('K001', 'Ainun', 'Ainun@gmail.com', '929839292927', 'Solok', 'Perempuan', '2020-10-02', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_login`
--

CREATE TABLE `tb_login` (
  `id` int(11) NOT NULL,
  `id_user` varchar(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_login`
--

INSERT INTO `tb_login` (`id`, `id_user`, `email`, `password`, `level`) VALUES
(5, '1409369599', 'budibudi@gmail.com', '1234', 'admin'),
(23, 'K001', 'Ainun@gmail.com', '123', 'konsumen'),
(24, 'P001', 'Handoko@gmail.com', '123', 'petani');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pembayaran`
--

CREATE TABLE `tb_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_pemesanan` varchar(30) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `file` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `id_pemesanan`, `jumlah_bayar`, `tanggal`, `file`, `status`) VALUES
(2, 'KPM001', 100000, '2020-10-02', '1601647033.jpeg', 1),
(3, 'KPM002', 200000, '2020-10-02', '1601647236.jpeg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `id` int(11) NOT NULL,
  `id_pemesanan` varchar(30) NOT NULL,
  `id_petani` varchar(20) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_konsumen` varchar(20) NOT NULL,
  `jumlah_pemesanan` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_petani`
--

CREATE TABLE `tb_petani` (
  `id_petani` varchar(20) NOT NULL,
  `nm_petani` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `alamat` text NOT NULL,
  `jekel` varchar(20) NOT NULL,
  `tgl_bergabung` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_petani`
--

INSERT INTO `tb_petani` (`id_petani`, `nm_petani`, `email`, `no_telepon`, `alamat`, `jekel`, `tgl_bergabung`, `status`) VALUES
('P001', 'Handoko', 'Handoko@gmail.com', '8382226728382', 'Solok', 'Laki-Laki', '2020-10-02', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id_produk` int(11) NOT NULL,
  `id_petani` varchar(20) NOT NULL,
  `nm_produk` varchar(100) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL,
  `min_pemesanan` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `tgl_upload` date NOT NULL,
  `tgl_expired` date NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `id_petani`, `nm_produk`, `harga_produk`, `satuan`, `min_pemesanan`, `stok`, `tgl_upload`, `tgl_expired`, `deskripsi`, `gambar`) VALUES
(11, 'P001', 'Sayur Bakyam', 200000, 'Kg', 10, 200, '2020-10-05', '2020-12-31', 'Sayurnya segar', 'images.jpg'),
(12, 'P001', 'Cabai', 200000, 'Kg', 10, 700, '2020-10-05', '2020-12-31', 'Segar', 'Indonesian_vegetables.jpg'),
(13, 'P001', 'Bawang India', 59000, 'Kg', 10, 200, '2020-10-05', '2020-12-31', 'Segar', 'fresh-2462362_1280.webp'),
(14, 'P001', 'Sayuran Sawah', 5600, 'Kg', 10, 40, '2020-10-05', '2020-12-31', 'Segar', 'sayuran_tetap_segar_HERO.original.jpegquality-90.jpg'),
(15, 'P001', 'Kacang-Kacangan', 200000, 'Kg', 2, 50, '2020-10-05', '2020-12-31', 'Segar', 'fresh-2462362_1280.webp'),
(16, 'P001', 'Umbi-Umbian', 5000, 'Kg', 40, 200, '2020-10-05', '2020-12-31', 'Segar', 'sayuran_tetap_segar_HERO.original.jpegquality-90.jpg'),
(17, 'P001', 'Tomat', 4500, 'Kg', 3, 120, '2020-10-05', '2020-12-31', 'Segar', '2019-01-23-09-58-51.jpg'),
(18, 'P001', 'Bakyam', 4500, 'Kg', 2, 34, '2020-10-05', '2020-12-31', 'Segar', 'xselada,P20,P281,P29.jpg.pagespeed.ic.u2AuxVFEKf.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `tb_konsumen`
--
ALTER TABLE `tb_konsumen`
  ADD PRIMARY KEY (`id_konsumen`);

--
-- Indeks untuk tabel `tb_login`
--
ALTER TABLE `tb_login`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indeks untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tb_petani`
--
ALTER TABLE `tb_petani`
  ADD PRIMARY KEY (`id_petani`);

--
-- Indeks untuk tabel `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tb_login`
--
ALTER TABLE `tb_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
