    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Keranjang</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Keranjang</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

    <!-- Start Cart  -->
    <div class="cart-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-main table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Gambar</th>
                                    <th>Nama produk</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Sub Total</th>
                                    <th>Edit Jumlah</th>
                                    <th></th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $bag=mysqli_query($koneksi,"SELECT * FROM tb_pemesanan WHERE status=0 AND id_konsumen='$id'");
                            foreach ($bag as $data_keranjang) {
                        
                            $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk='$data_keranjang[id_produk]'");
                            foreach ($produk as $dt_produk) {
                                # code...
                            }
                               
                            
                             ?>
                                <tr>
                                    <td><img width="68px" src="page/petani/foto-produk/<?= $dt_produk['gambar']  ?>"></td>
                                   <td><?= $dt_produk['nm_produk']  ?></td>
                                   <td><?= rupiah($dt_produk['harga_produk'])  ?></td>
                                   <td><?= $data_keranjang['jumlah_pemesanan']  ?></td>
                                   <td><?= rupiah($data_keranjang['total_harga'])  ?></td>
                                   <td>
                                    <form action="aksi/edit-jumlah.php" method="POST">

                                    <input type="" hidden="" name="id" value="<?= $data_keranjang['id']  ?>">
                                    <input type="" hidden="" name="harga" value="<?= $dt_produk['harga_produk']  ?>">
                                   <input value="<?= $data_keranjang['jumlah_pemesanan']  ?>" name="jumlah" class="form-control" required="" value="0" min="<?= $dt_produk['min_pemesanan']  ?>" max="<?= $dt_produk['stok']  ?>" type="number">
                                    
                                    <td><button type="submit" class="btn btn-danger">Ubah</button></td>
                                    </form>
                                    </td>
                                   <td><a href="hapus-keranjang&id=<?= $data_keranjang['id']  ?>"><i class="fa fa-times"></i></a></td>
                                </tr>
                            <?php } ?>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row my-5">
                <div class="col-lg-8 col-sm-12"></div>
                <div class="col-lg-4 col-sm-12">
                    <div class="order-box">
                        <h3>Pemesanan</h3>
                        <div class="d-flex gr-total">
                            <h5>Grand Total</h5>
                            <?php 
                                $total_harga=mysqli_query($koneksi,"SELECT sum(total_harga) as total FROM tb_pemesanan WHERE status=0 AND id_konsumen='$id'");
                                foreach ($total_harga as $dt_total) {
                                    # code...
                                }
                             ?>
                                
                            <div class="ml-auto h5"><?= rupiah($dt_total['total'])  ?></div>
                          
                        </div>
                        <hr> </div>
                <form action="aksi/cekout-keranjang.php" method="POST">
                    <input hidden="" type="" value="<?= $dt_produk['id_produk']  ?>" name="id_produk">
                    <input hidden="" type="" value="<?= $id; ?>" name="id_konsumen">
                    <button type="submit" class="btn hvr-hover" style="color: #ffffff">Chekout</button>
                </form>
                </div>
            </div>

        </div>
    </div>
    <!-- End Cart -->