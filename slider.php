<div id="slides-shop" class="cover-slides">
        <ul class="slides-container">
            <li class="text-center">
                <img src="images/banner-02.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="m-b-20"><strong>Selamat datang di <br> Pasar Sayur</strong></h1>
                            <p class="m-b-40">Lihat berbagai kebutuhan sayur anda di website ini<br> sayur segar langsung dari para petani.</p>
                            <p><a class="btn hvr-hover" href="#">Belanja Sekarang</a></p>
                        </div>
                    </div>
                </div>
            </li>
            <li class="text-center">
                <img src="images/banner-03.jpg" alt="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                           <h1 class="m-b-20"><strong>Selamat datang di <br> Pasar Sayur</strong></h1>
                            <p class="m-b-40">Lihat berbagai kebutuhan sayur anda di website ini<br> sayur segar langsung dari para petani.</p>
                            <p><a class="btn hvr-hover" href="#">Belanja Sekarang</a></p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="slides-navigation">
            <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
            <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
        </div>
    </div>