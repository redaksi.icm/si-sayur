<?php 

if(isset($_GET['p'])){
		$page = $_GET['p'];
 
		switch ($page) {
			case 'masuk':
				include "form-login.php";
				break;
			case 'buat-akun':
				include "buat-akun.php";
				break;
			case 'tentang':
				include "tentang.php";
				break;
			case 'konsumen':
				include "page/konsumen/akun-saya.php";
				break;
			case 'keranjang':
				include "keranjang.php";
				break;
			case 'detail-produk':
				include "detail-produk.php";
				break;
			case 'checkout':
				include "checkout.php";
				break;
			case 'toko-petani':
				include "page/petani/toko-petani.php";
				break;
			case 'petani':
				include "page/petani/akun-petani.php";
				break;
			case 'tambah-produk':
				include "page/petani/tambah_produk.php";
				break;
			case 'hapus-produk':
				include "page/petani/aksi/hapus_sayur.php";
				break;
			case 'edit-produk':
				include "page/petani/edit-sayur.php";
				break;
			case 'profil-petani':
				include "page/petani/profil.php";
				break;
			case 'profil-konsumen':
				include "page/konsumen/profil.php";
				break;
			case 'pemesanan-konsumen':
				include "page/konsumen/pemesanan.php";
				break;
			case 'upload-pembayaran':
				include "page/konsumen/upload-pembayaran.php";
				break;

			case 'hapus-keranjang':
				include "aksi/hapus-keranjang.php";
				break;
			
			case 'print-invoice':
				include "page/konsumen/aksi/print-invoice.php";
				break;

			case 'hapus-pesanan':
				include "page/konsumen/aksi/hapus-pemesanan.php";
				break;
			case 'riwayat-pemesanan':
				include "page/konsumen/riwayat_pemesanan.php";
				break;

			case 'pemesanan':
				include "page/petani/pemesanan-konsumen.php";
				break;
			case 'cara-pembayaran':
				include "cara-pembayaran.php";
				break;
			
						
			// default:
			// 	include"error.php";
			// 	break;
		}
	}else{

		include"slider.php";
		include"list-produk.php";
		
		
	}

 ?>