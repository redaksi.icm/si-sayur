<?php 
if ($level=='petani' OR $level=='admin') {
    echo "<script>
                setTimeout(function(){
                        Swal.fire({
                            icon:'error',
                            title:'Anda tidak dapat melakukan pemesanan, silahkan login sebagai konsumen',
                            type:'error',
                            timer:'3000',
                            showConfrimButton:'true'
                            });
                    },10);
                    window.setTimeout(function(){
                        window.location.replace('masuk');
                        },3000);
            </script>";
}

 ?>
 <?php 
 $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk='$_GET[id]'");
foreach ($produk as $dt_produk) {
    
}
$petani=mysqli_query($koneksi,"SELECT * FROM tb_petani where id_petani='$dt_produk[id_petani]'");
foreach ($petani as $dt_petani) {
    # code...
}


  ?>
<!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Detail Produk</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Detail Produk</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->

<div class="shop-detail-box-main">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-6">
                    <img class="d-block w-100" width="100%" height="500px" src="page/petani/foto-produk/<?= $dt_produk['gambar']  ?>" alt="First slide">
                </div>
                <div class="col-xl-7 col-lg-7 col-md-6">
                    <div class="single-product-details">
                        <h2><?= $dt_produk['nm_produk']  ?></h2>
                        <h5><?= rupiah($dt_produk['harga_produk'])  ?>/<?= $dt_produk['satuan']  ?></h5>
                        <p class="available-stock"><span> Minimal Pemesanan: <?= $dt_produk['min_pemesanan']  ?><?= $dt_produk['satuan']  ?></span></p>
                        <p class="available-stock"><span> Stok Tersedia: <?= $dt_produk['stok']  ?><?= $dt_produk['satuan']  ?></span></p>
                        <p>Petani : <?= $dt_petani['nm_petani']  ?> <br> <span><a href="toko-petani&id=<?= $dt_petani['id_petani']  ?>" style="color: red"> Lihat Toko</a></span></p>
                        <hr>
                        <h4>Deskripsi Produk:</h4>
						<p><?= $dt_produk['deskripsi']  ?></p>
                        <form action="aksi/tambah-pemesanan.php" method="POST">
                        <input type="text" hidden="" name="id_konsumen" value="<?= $id;  ?>">
                        <input type="text" hidden="" name="id_produk" value="<?= $dt_produk['id_produk']  ?>">

						<ul>
							<li>
								<div class="form-group quantity-box">
									<label class="control-label">Jumlah Pemesanan</label>
									<input name="jumlah" class="form-control" required="" value="0" min="<?= $dt_produk['min_pemesanan']  ?>" max="<?= $dt_produk['stok']  ?>" type="number">
								</div>
							</li>
						</ul>

						<div class="price-box-bar">
							<div class="cart-and-bay-btn">
                                 <a type="" style="color: red" data-fancybox-close="" href="index.php">Batal</a>
								<button name="pesan" style="color: white" class="btn hvr-hover" type="submit" data-fancybox-close="" href="checkout">Pesan Sekarang</button>
                            
                        </form>
							</div>
						</div>
                    </div>
                </div>
            </div>
			
			<div class="row my-5">
				<div class="card card-outline-secondary my-4">
					<div class="card-header">
						<h2>Cara Pemesanan</h2>
					</div>
					<div class="card-body">
						<div class="media mb-3">
							<div class="media-body">
								<p>1. Pemesanan hanya dapat dilakukan dari jam 07:00 s&d 17:00</p>
							</div>
						</div>
						<hr>
                        <div class="media mb-3">
                            <div class="media-body">
                                <p>2. Pemesanan dapat dilakukan sesuai dengan minimal pembelian</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media mb-3">
                            <div class="media-body">
                                <p>3. Setelah melakukan pemesanan konsumen segera melakukan pembayaran kepada rekening tujuan dan mengupload bukti pembayaran</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media mb-3">
                            <div class="media-body">
                                <p>4. Konsumen dapat mengambil pesanan kepada petani 5 Jam setelah pemesanan dilakukan.</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media mb-3">
                            <div class="media-body">
                                <p>5. Jika telah dilakukan pembayaran pemesanan tidak bisa dibatalkan.</p>
                            </div>
                        </div>
                        <hr>
                        
					</div>
				  </div>
			</div>
        </div>
    </div>