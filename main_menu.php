    <!-- Start Main Top -->
    <div class="main-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="right-phone-box">
                        <p>Telepon :- <a href="#"> +62 800 800 100</a></p>

                    </div>
                    <div class="our-link">
                        <ul>
                            <?php if ($id==true): ?>
                                
                            <li><a href="<?= $level  ?>&id=<?= $id  ?>"><i class="fa fa-user s_color"></i> Akun Saya</a></li>
                            <li><a href="keluar.php"><i class="fa fa-sign-out-alt s_color"></i> Keluar</a></li>
                            <?php endif ?>
                            <?php if ($id==false): ?>
                                
                            <li><a href="buat-akun"><i class="fa fa-book s_color"></i> Buat Akun</a></li>
                            <li><a href="masuk"><i class="fa fa-sign-in-alt s_color"></i> Masuk</a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Top -->
<header class="main-header">
        <!-- Start Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-default bootsnav">
            <div class="container">
                <!-- Start Header Navigation -->
                <div class="navbar-header">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-controls="navbars-rs-food" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                    <a class="navbar-brand" href="index.php"><h2 style="font-size: 48px; font-weight: 100%; color: #eac228; font-family: 'Dosis', sans-serif;"><strong>Pasa</strong>Sayua</h2>
                        <h2 style="margin-top: -20px; font-family: 'Dosis';color: #00BFFF;">Kampung Batu Dalam</h2></a>
                </div>
                <!-- End Header Navigation -->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-menu">
                    <ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="tentang">Tentang Kami</a></li>
                        <li class="nav-item"><a class="nav-link" href="cara-pembayaran">Cara Pembayaran</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->

                <!-- Start Atribute Navigation -->
                    
                    
              
                <div class="attr-nav">
                    <ul>
                        <li class="search"><a href=""><i class="fa fa-search"></i></a></li>
                     <?php  
                     if ($level=='konsumen'&&$id==true) {
                          
                     ?>
                        <li class="side-menu">
							<a href="keranjang">
								<i class="fa fa-shopping-bag"></i>
                                <?php 
                                    $angka=mysqli_query($koneksi,"SELECT count(id_pemesanan) as angka FROM tb_pemesanan WHERE status=0 AND id_konsumen='$id'");
                                  foreach ($angka as $data) {
                                      # code...
                                  }

                                 ?>
								<span class="badge"><?= $data['angka']  ?></span>
								<p>Keranjang Saya</p>
							</a>
						</li>
                    <?php } ?>
                
                    </ul>
                </div>

                <!-- End Atribute Navigation -->
            </div>
            <!-- Start Side Menu -->
            <div class="side">
                <a href="#" class="close-side"><i class="fa fa-times"></i></a>
                <li class="cart-box">
                    <ul class="cart-list">
                        <?php 
                            $keranjang=mysqli_query($koneksi,"SELECT * FROM tb_pemesanan WHERE status=0 AND id_konsumen='$id'");
                            foreach ($keranjang as $dt_keranjang) {

                            $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE id_produk=$dt_keranjang[id_produk]");
                            foreach ($produk as $dt_produk) {
                                # code...
                            }
                               
                            
                         ?>
                        <li>
                            <a href="#" class="photo"><img src="page/petani/foto-produk/<?= $dt_produk['gambar']  ?>" class="cart-thumb" alt="" /></a>
                            <h6><a href="#"><?= $dt_produk['nm_produk']  ?></a></h6>
                            <p><?= $dt_keranjang['jumlah_pemesanan']  ?> X <span class="price"><?= rupiah($dt_produk['harga_produk'])  ?></span></p>
                        </li>
                        <?php } ?>
                        <li>
                        <?php if (empty($dt_keranjang)): ?>
                            <h4 align="center">Kosong</h4>
                        <?php endif ?>
                        </li>
                        
                        <li class="total">
                            <a href="keranjang" class="btn btn-default hvr-hover btn-cart">Lihat</a>
                            <?php 
                                $total_harga=mysqli_query($koneksi,"SELECT sum(total_harga) as total FROM tb_pemesanan WHERE status=0 AND id_konsumen='$id'");
                                foreach ($total_harga as $dt_total) {
                                    # code...
                                }
                             ?>
                            <span class="float-right"><strong>Total</strong>: <?= rupiah($dt_total['total'])  ?></span>
                        </li>
                    </ul>
                </li>
            </div>
            <!-- End Side Menu -->
        </nav>
        <!-- End Navigation -->
    </header>