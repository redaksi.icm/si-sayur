    <!-- Start All Title Box -->
    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Checkout</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Checkout</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End All Title Box -->
    <!-- Start Cart  -->
    <div class="cart-box-main">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12 mb-3">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="odr-box">
                                <div class="title-left">
                                    <h3>Detail Pemesanan</h3>
                                </div>
                                <div class="rounded p-2 bg-light">
                                    <div class="media mb-2 border-bottom">
                                        <div class="media-body"> <a href="detail.html"> Nama Sayur</a>
                                            <div class="small text-muted">Harga: Rp.20.000/Kg <span class="mx-2">|</span> Jumlah: 100kg <span class="mx-2">|</span> Subtotal: Rp.1.080.000</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="order-box">
                                <div class="title-left">
                                    <h3>Detail Pembayaran</h3>
                                </div>
                                <div class="d-flex">
                                    <div class="font-weight-bold">Produk</div>
                                    <div class="ml-auto font-weight-bold">Total</div>
                                </div>
                                <hr class="my-1">
                                <div class="d-flex">
                                    <h4>Sub Total</h4>
                                    <div class="ml-auto font-weight-bold"> Rp.1.000.000</div>
                                </div>
                                <div class="d-flex">
                                    <h4>Biaya Layanan</h4>
                                    <div class="ml-auto font-weight-bold"> Rp.50.000</div>
                                </div>
                                <hr class="my-1">
                                <div class="d-flex gr-total">
                                    <h5>Grand Total</h5>
                                    <div class="ml-auto h5"> Rp. 1.050.000</div>
                                </div>
                                <hr> 
                            </div>
                        </div>
                        <div class="col-12 d-flex shopping-box"> 
                            <a style="color: #ffffff" href="" class="ml-auto btn hvr-hover">Kembali</a> 
                            <button style="color: #ffffff" type="submit" class="ml-auto btn hvr-hover">Konfirmasi Pesanan</button> 
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- End Cart -->

