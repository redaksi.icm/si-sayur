    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Masuk</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Masuk</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">

                    
                </div>
                <div class="col-lg-6 col-sm-12">

                    <div class="contact-form-right">
                        <h2 align="center">MASUK</h2>

                        <p align="center">Silahkan masukan akun yang terdaftar.</p>
                        <?php if (isset($_GET['akun-belum-aktif'])): ?>
                            
                         <div class="alert alert-danger alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <h5><i class="icon fas fa-info"></i> Alert!</h5>
                           Mohon Maaf akun anda belum aktif, tunggu konfirmasi dari kami!.
                        </div>
                        <?php endif ?>

                            <form  action="action_login.php" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  name="email" placeholder="Email" required="" data-error="Masukan email yang terdaftar">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="password" placeholder="Kata Sandi" class="form-control" name="password" required="" data-error="Masukan Password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" name="level">
                                                <option selected="" value="petani">Saya Petani</option>
                                                <option value="konsumen" >Saya Konsumen</option>
                                            </select>    
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <a class="btn hvr-hover disabled" href="index.php" style="pointer-events: all; cursor: pointer;color: white;">Batal</a>
                                        <button class="btn hvr-hover disabled" id="submit" type="submit" style="pointer-events: all; cursor: pointer;color: white;">Masuk</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            
                        </div>
                    </div>
                </div>
            </div>


