    <div class="all-title-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Buat Akun</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Buat Akun</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">   
                </div>
                <div class="col-lg-6 col-sm-12">
                    <div class="contact-form-right">
                        <div class="alert alert-info alert-dismissible">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                          <h5><i class="icon fas fa-info"></i> Alert!</h5>
                          Buat akun sesuai kebutuhan anda.
                        </div>

                        <h2 align="center">Buat Akun</h2>
                        <p align="center">Silahkan isi form di bawah ini dengan benar.</p>
                            <form  action="action_daftar.php" method="post">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  name="nama" placeholder="Nama Lengkap" required="">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  name="email" placeholder="Email" required="" data-error="Masukan email yang terdaftar">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control"  name="no_telepon" placeholder="Nomor Telepon" required="">
                                        </div>
                                    </div>
                                     <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" name="alamat" required="" placeholder="Alamat"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" name="jekel">
                                                <option selected="" value="Laki-Laki">Laki-Laki</option>
                                                <option value="Perempuan" >Perempuan</option>
                                            </select>    
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" name="level">
                                                <option selected="" value="petani">Saya Petani</option>
                                                <option value="konsumen" >Saya Konsumen</option>
                                            </select>    
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="taxt" placeholder="Kata Sandi" id="password" class="form-control" name="password" required="" data-error="Masukan Password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn hvr-hover disabled" id="submit" type="submit" style="pointer-events: all; cursor: pointer;color: white;">Buat Akun</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            
                        </div>
                    </div>
                </div>
            </div>


