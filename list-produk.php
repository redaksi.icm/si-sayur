<?php 

$produk=mysqli_query($koneksi,"SELECT * FROM tb_produk ORDER BY tgl_upload  DESC");
foreach ($produk as $dt_produk) {
   

$petani=mysqli_query($koneksi,"SELECT * FROM tb_petani where id_petani='$dt_produk[id_petani]'");
foreach ($petani as $dt_petani) {
    # code...
}
}

 ?>
<div class="products-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title-all text-center">
                        <h1>Produk</h1>
                        <p>Dapatkan sayuran langsung dari para petani.</p>
                    </div>
                </div>
            </div>
            <div class="row special-list">
            <?php 
            $tgl_now=date('Y-m-d');
                $produk=mysqli_query($koneksi,"SELECT * FROM tb_produk WHERE tgl_expired >= '$tgl_now' AND stok > 0 ORDER BY tgl_upload DESC");
                foreach ($produk as $dt_produk) {
                    
                
             ?>
                <div class="col-lg-3 col-md-6 special-grid best-seller">
                    <div class="products-single fix">
                        <div class="box-img-hover">
                            <img height="200px" width="100%"  src="page/petani/foto-produk/<?= $dt_produk['gambar']  ?>"  alt="Image">
                        </div>
                        <div class="why-text">
                            <h4><?= $dt_produk['nm_produk']  ?></h4>
                            <h5> <?= rupiah($dt_produk['harga_produk'])  ?>/<?= $dt_produk['satuan']  ?></h5>
                            <p>Minimal pembelian: <?= $dt_produk['min_pemesanan']  ?><?= $dt_produk['satuan']  ?></p>
                            <p>Stok Tersedia: <?= $dt_produk['stok']  ?><?= $dt_produk['satuan']  ?></p>
                            <p>Tanggal Expired: <?= $dt_produk['tgl_expired']  ?></p>
                            <p>Petani: <i class="fa fa-user"></i><a href="toko-petani&id=<?= $dt_petani['id_petani']  ?>" style="color: blue"> <?= $dt_petani['nm_petani']  ?></a></p>
                            <div class="price-box-bar">
                            <div class="cart-and-bay-btn">
                            <form action="aksi/tambah_keranjang.php" method="POST">
                                <input hidden="" type="" value="<?= $id  ?>" name="id_konsumen">
                                <input hidden="" type="" value="<?= $dt_produk['id_produk']  ?>" name="id_produk">
                                <input hidden="" type="" value="<?= $dt_produk['min_pemesanan']  ?>" name="jumlah">
                                <?php if ($level=='konsumen'): ?>
                                    
                                <button style="color: #ffffff" class="btn hvr-hover" type="submit" data-fancybox-close=""><i class="fa fa-shopping-bag"></i></button>
                                <?php endif ?>
                                <a class="btn hvr-hover" data-fancybox-close="" href="detail-produk&id=<?= $dt_produk['id_produk']  ?>">Pesan Sekarang</a>
                            </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        <?php } ?>
            </div>
        </div>
    </div>